const dispatchUtil = {
    eventsToDispatch: [
        'add-to-cart',
        'delete-from-cart',
    ],
    getEventFromList: function(currentOBJ) {
        let eventList = [...this.eventsToDispatch];
        let clickedItem = currentOBJ.classList.value;
        let doesEventList = eventList.includes(clickedItem);
        if (!doesEventList) {
            return;
        }
        let eventToPassIndex = eventList.indexOf(clickedItem);
        let eventToPass = eventList[eventToPassIndex];
        return eventToPass;
    },
    dispatchEvent: function(currentOBJ,dispatchFunc) {
        let event = this.getEventFromList(currentOBJ);
        dispatchFunc(event);
    }
};
export default dispatchUtil;