# **Svelte Event Dispatch Utility (SEDU) for Custom Events**
### **Manage your custom svelte events in a single place.**


## **SetUp - In your desired component(s)**
### You need the DispatchUtil.js file, but you can put it anywhere. Import the DispatchUtil.js into any component where you want to add a custom event. Make sure to also import createEventDispatcher from svelte:
```
import { createEventDispatcher } from 'svelte';
import dispatchUtil from './DispatchUtil/DispatchUtil.js'
```
### Add your elements with custom events, in the example I'm using two buttons. Add your custom event names as classes for the elements with custom events:
```
<button class="add-to-cart">
    Add to Cart
</button>

<button class="delete-from-cart">
    Delete
</button>
```
### Add the custom functionality to the buttons:
```
  <button 
    class="add-to-cart" 
    on:click={function (){
        dispatchUtil.dispatchEvent(this,dispatch);
    }}>
    Add to Cart
    </button>
    <button 
    class="delete-from-cart" 
    on:click={function (){
        dispatchUtil.dispatchEvent(this,dispatch);
    }}>
    Delete
    </button>
```
## **SetUp - In DispatchUtil.js**
### Add the names of your custom events to the eventToDispatch object like so:
```
eventsToDispatch: [
    'add-to-cart',
    'delete-from-cart',
],
```

## **SetUp - In the file where your component is called**

### In the included files my custom component is Product.svelte and it's called within App.svelte. The svelte components I'm using in the example files are arbitrary and may be unrelated to your project but the functionality SEDU remains the same. 

Import your component:
```
import Product from './Product.svelte';
```

### Add your component to your markup. Any elements in your imported component that have custom events are referrenced like so:
```
<Product 
{productTitle} 
on:add-to-cart={() => alert("added to cart")} 
on:delete-from-cart={() => alert("removed from cart")} 
/>
```

## TODO
*Simplfy file structure*

*Clean up code*

*Separate eventsToDispatch from code (needs to be user added where my code looks for it)*

*Add to NPM*